import { browser } from '@wdio/globals';
import { completeAuth, verifyDuoWorkflowStartButton } from '../helpers/index.js';

// Please note that Duo Workflow feature is controlled by a feature flag and
// currently only available for internal GitLab team members for testing.
// See https://docs.gitlab.com/ee/user/duo_workflow/ for more information.
describe('GitLab Workflow Extension Duo Workflow', async () => {
  let workbench;

  before(async () => {
    await completeAuth();
  });

  it('navigates to Duo Workflow tab', async () => {
    workbench = await browser.getWorkbench();
    await workbench.executeCommand('Gitlab: Show Duo Workflow');
    const duoWorkflowView = await workbench.getWebviewByTitle('GitLab Duo Workflow');

    await duoWorkflowView.open();
    await verifyDuoWorkflowStartButton;
  });
});
