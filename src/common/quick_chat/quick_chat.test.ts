import vscode from 'vscode';
import { Cable } from '@anycable/core';
import { SPECIAL_MESSAGES } from '../chat/constants';
import { GitLabPlatformManagerForChat } from '../chat/get_platform_manager_for_chat';
import { AIContextManager } from '../chat/ai_context_manager';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabChatApi } from '../chat/gitlab_chat_api';
import {
  createFakeWorkspaceConfiguration,
  createConfigurationChangeTrigger,
} from '../test_utils/vscode_fakes';
import { QuickChat } from './quick_chat';
import {
  COMMAND_CLOSE_QUICK_CHAT,
  COMMAND_OPEN_QUICK_CHAT,
  COMMAND_OPEN_QUICK_CHAT_WITH_SHORTCUT,
  COMMAND_QUICK_CHAT_MESSAGE_TELEMETRY,
  COMMAND_QUICK_CHAT_OPEN_TELEMETRY,
  COMMAND_SEND_QUICK_CHAT,
  COMMAND_SEND_QUICK_CHAT_DUPLICATE,
  QUICK_CHAT_OPEN_TRIGGER,
} from './constants';
import * as utils from './utils';
import { COMMENT_CONTROLLER_ID, generateThreadLabel } from './utils';

jest.mock('../chat/gitlab_chat_api');

const mockCommentController = createFakePartial<vscode.CommentController>({
  createCommentThread: jest.fn(),
  dispose: jest.fn(),
});

jest.mock('./utils', () => {
  const originalModule = jest.requireActual('./utils');
  const mockModule = jest.createMockFromModule<typeof originalModule>('./utils');

  return {
    ...mockModule,
    MarkdownProcessorPipeline: jest.fn().mockImplementation(() => ({
      process: jest.fn(md => md),
    })),
    createCommentController: jest.fn(() => mockCommentController),
    generateThreadLabel: jest.fn(() => 'Duo Quick Chat'),
  };
});

const mockDebug = jest.fn();

jest.mock('../log', () => {
  const originalModule = jest.requireActual('../log');
  const mockModule = jest.createMockFromModule<typeof originalModule>('../log');

  return {
    ...mockModule,
    log: {
      debug: jest.fn().mockImplementation(msg => mockDebug(msg)),
      log: jest.fn(),
      error: jest.fn(),
      warn: jest.fn(),
    },
  };
});

describe('QuickChat', () => {
  let quickChat: QuickChat;
  let mockManager: GitLabPlatformManagerForChat;
  let mockApi: jest.Mocked<GitLabChatApi>;
  let mockConfig: vscode.WorkspaceConfiguration;
  let mockThread: vscode.CommentThread;
  let mockAiContextManager: AIContextManager;
  let mockExtensionContext: vscode.ExtensionContext;

  beforeEach(() => {
    mockManager = createFakePartial<GitLabPlatformManagerForChat>({
      getProjectGqlId: jest.fn().mockResolvedValue('mock-project-id'),
      getGitLabPlatform: jest.fn().mockResolvedValue(undefined),
      getGitLabEnvironment: jest.fn().mockResolvedValue(undefined),
    });

    mockApi = {
      clearChat: jest.fn().mockResolvedValue({}),
      resetChat: jest.fn().mockResolvedValue({}),
      subscribeToUpdates: jest.fn(),
      processNewUserPrompt: jest.fn(),
    } as unknown as jest.Mocked<GitLabChatApi>;

    (GitLabChatApi as jest.MockedClass<typeof GitLabChatApi>).mockImplementation(() => mockApi);

    mockConfig = createFakeWorkspaceConfiguration({
      keybindingHints: { enabled: true },
    });
    jest.spyOn(vscode.workspace, 'getConfiguration').mockReturnValue(mockConfig);

    mockThread = createFakePartial<vscode.CommentThread>({
      comments: [],
      range: new vscode.Range(0, 0, 0, 0),
    });
    mockAiContextManager = createFakePartial<AIContextManager>({});
    mockExtensionContext = createFakePartial<vscode.ExtensionContext>({});
    quickChat = new QuickChat(mockManager, mockExtensionContext, mockAiContextManager);
  });

  const triggerTextEditorSelectionChange = (e: vscode.TextEditorSelectionChangeEvent) => {
    jest
      .mocked(vscode.window.onDidChangeTextEditorSelection)
      .mock.calls.forEach(([listener]) => listener(e));
  };

  async function showChatWithEditor(userInput = 'Test question') {
    const mockUri = vscode.Uri.file('/path/to/file.ts');
    const mockPosition = new vscode.Position(0, 0);
    const mockRange = new vscode.Range(mockPosition, mockPosition);
    const mockEditor = createFakePartial<vscode.TextEditor>({
      document: { uri: mockUri },
      selection: mockRange,
      setDecorations: jest.fn(),
    });
    (vscode.window.activeTextEditor as unknown) = mockEditor;

    (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

    jest.spyOn(vscode.window, 'showInputBox').mockResolvedValue(userInput);

    await quickChat.show();

    return { mockUri, mockPosition, mockRange, mockEditor };
  }

  it('registers commands', () => {
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_OPEN_QUICK_CHAT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_SEND_QUICK_CHAT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_SEND_QUICK_CHAT_DUPLICATE,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_OPEN_QUICK_CHAT_WITH_SHORTCUT,
      expect.any(Function),
    );
    expect(vscode.commands.registerCommand).toHaveBeenCalledWith(
      COMMAND_CLOSE_QUICK_CHAT,
      expect.any(Function),
    );
  });

  it('disposes commands', () => {
    const disposeHandler = jest.fn();
    jest.mocked(vscode.commands.registerCommand).mockReturnValue({ dispose: disposeHandler });
    quickChat = new QuickChat(mockManager, mockExtensionContext, mockAiContextManager);
    quickChat.dispose();
    expect(disposeHandler).toHaveBeenCalledTimes(5);
  });

  describe('show', () => {
    it('creates a new comment thread when an editor is active', async () => {
      const { mockUri, mockRange } = await showChatWithEditor();

      expect(mockApi.clearChat).toHaveBeenCalled();
      expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
      expect(mockCommentController.createCommentThread).toHaveBeenCalledWith(
        mockUri,
        mockRange,
        [],
      );
      expect(mockThread.collapsibleState).toBe(vscode.CommentThreadCollapsibleState.Expanded);
      expect(mockThread.label).toBe('Duo Quick Chat');
    });

    it('does nothing when no editor is active', async () => {
      (vscode.window.activeTextEditor as unknown) = undefined;

      await quickChat.show();

      expect(mockApi.clearChat).not.toHaveBeenCalled();
    });

    it('sends user input when provided through input box', async () => {
      jest.spyOn(quickChat, 'send');
      const userInput = 'This is a test question';
      await showChatWithEditor(userInput);

      expect(vscode.window.showInputBox).toHaveBeenCalledWith({
        placeHolder: 'Ask a question or give an instruction...',
        title: 'GitLab Duo Quick Chat',
      });

      expect(quickChat.send).toHaveBeenCalledWith({ text: userInput, thread: mockThread });
    });

    it('tracks telemetry event for chat open', async () => {
      await showChatWithEditor();

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        COMMAND_QUICK_CHAT_OPEN_TELEMETRY,
        { trigger: QUICK_CHAT_OPEN_TRIGGER.CLICK },
      );
    });

    it('provides commenting ranges', async () => {
      await showChatWithEditor();
      expect(mockCommentController.commentingRangeProvider).not.toBeUndefined();
    });
  });

  describe('send', () => {
    let mockReply: vscode.CommentReply;
    let updateHandler: jest.Mock;

    beforeEach(() => {
      mockReply = createFakePartial<vscode.CommentReply>({
        thread: mockThread,
        text: 'Test question',
      });

      updateHandler = jest.fn();
      mockApi.subscribeToUpdates.mockImplementation(handler => {
        updateHandler.mockImplementation(handler);
        return Promise.resolve({} as Cable);
      });

      (utils.createComment as jest.Mock).mockImplementation(text => ({ body: text }));

      return showChatWithEditor();
    });

    it('handles full response correctly', async () => {
      const responseText = 'Full response text';
      await updateHandler({ content: responseText });

      expect(mockThread.comments[2].body).toEqual(expect.objectContaining({ value: responseText }));
    });

    it('handles chunked responses correctly', async () => {
      const mockAppendMarkdown = jest.fn();
      const mockMarkdown = {
        appendMarkdown: mockAppendMarkdown,
        value: '',
      };
      (vscode.MarkdownString as jest.Mock) = jest.fn(() => mockMarkdown);

      await quickChat.send(mockReply);
      await updateHandler({ chunkId: 1, content: 'Chunk 1 ' });
      await updateHandler({ chunkId: 2, content: 'Chunk 2 ' });

      expect(mockAppendMarkdown).toHaveBeenNthCalledWith(1, 'Chunk 1 ');
      expect(mockAppendMarkdown).toHaveBeenNthCalledWith(2, 'Chunk 2 ');
    });

    it('handles /clear command correctly', async () => {
      mockReply.text = SPECIAL_MESSAGES.CLEAR;
      await quickChat.send(mockReply);

      expect(mockApi.clearChat).toHaveBeenCalled();
      expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
      expect(mockThread.comments).toEqual([]);
    });

    it('handles /reset command correctly', async () => {
      jest
        .mocked(utils.createChatResetComment)
        .mockReturnValue(createFakePartial<vscode.Comment>({ body: 'reset' }));

      mockReply.text = SPECIAL_MESSAGES.RESET;
      await quickChat.send(mockReply);

      expect(mockApi.resetChat).toHaveBeenCalled();
      expect(utils.setLoadingContext).toHaveBeenCalledWith(false);
      expect(mockThread.comments).toContainEqual({ body: 'reset' });
    });

    it('tracks telemetry for sending messages', async () => {
      const userInput = 'Test message';
      mockReply.text = userInput;

      await quickChat.send(mockReply);

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        COMMAND_QUICK_CHAT_MESSAGE_TELEMETRY,
        { message: userInput },
      );
    });
  });

  describe('Quick Actions', () => {
    it('registers a completion item provider for quick actions', () => {
      expect(vscode.languages.registerCompletionItemProvider).toHaveBeenCalledWith(
        { scheme: 'comment', pattern: '**' },
        { provideCompletionItems: utils.provideCompletionItems },
        '/',
      );
    });
  });

  describe('thread label update', () => {
    const mockSelection = createFakePartial<vscode.Selection>({
      active: new vscode.Position(0, 0),
      start: new vscode.Position(0, 0),
      end: new vscode.Position(0, 0),
    });
    const mockChangeEvent = createFakePartial<vscode.TextEditorSelectionChangeEvent>({
      textEditor: {
        document: {
          lineAt: jest.fn(),
          uri: {
            scheme: 'file',
            authority: 'other',
          },
        },
        selection: mockSelection,
      },
    });

    const originalLabel = 'Original label';

    beforeEach(async () => {
      mockThread = createFakePartial<vscode.CommentThread>({
        label: originalLabel,
        range: new vscode.Range(0, 0, 0, 0),
      });
      const mockEditor = createFakePartial<vscode.TextEditor>({
        selection: mockSelection,
        document: {
          lineAt: jest.fn(),
          uri: createFakePartial<vscode.Uri>({}),
        },
        setDecorations: jest.fn(),
      });
      (vscode.window.activeTextEditor as unknown) = mockEditor;
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);
      await showChatWithEditor();
    });

    it('should update the thread label on selection change', async () => {
      const mockLabel = 'New selection for quick chat';
      jest.mocked(generateThreadLabel).mockReturnValue(mockLabel);

      await triggerTextEditorSelectionChange(mockChangeEvent);

      expect(generateThreadLabel).toHaveBeenCalled();
      expect(mockDebug).toHaveBeenCalledTimes(1);
      expect(mockThread.label).toBe(mockLabel);
    });

    it('does not update thread label when selection is un-changed', async () => {
      jest.mocked(generateThreadLabel).mockReturnValue(originalLabel);
      await triggerTextEditorSelectionChange(mockChangeEvent);

      mockDebug.mockClear();

      await triggerTextEditorSelectionChange(mockChangeEvent);

      expect(generateThreadLabel).toHaveBeenCalled();
      expect(mockDebug).not.toHaveBeenCalled();
      expect(mockThread.label).toBe(originalLabel);
    });

    it('should not update the label when the thread comment is in focus', () => {
      jest.mocked(generateThreadLabel).mockClear();
      triggerTextEditorSelectionChange(
        createFakePartial<vscode.TextEditorSelectionChangeEvent>({
          textEditor: {
            document: {
              lineAt: jest.fn(),
              uri: {
                scheme: 'comment',
                authority: COMMENT_CONTROLLER_ID,
              },
            },
            selection: mockSelection,
          },
        }),
      );
      expect(generateThreadLabel).not.toHaveBeenCalled();
    });
  });

  describe('keybinding hints', () => {
    let mockEditor: vscode.TextEditor;
    let mockDecorationType: vscode.TextEditorDecorationType;
    const triggerConfigurationChange = createConfigurationChangeTrigger();

    beforeEach(() => {
      mockEditor = createFakePartial<vscode.TextEditor>({
        selection: createFakePartial<vscode.Selection>({ active: new vscode.Position(0, 0) }),
        document: createFakePartial<vscode.TextDocument>({
          lineAt: jest.fn().mockReturnValue({ isEmptyOrWhitespace: true }),
          uri: vscode.Uri.file('/test/file.ts'), // Default to a file URI
        }),
        setDecorations: jest.fn(),
      });

      mockDecorationType = createFakePartial<vscode.TextEditorDecorationType>({
        dispose: jest.fn(),
      });
      jest.mocked(utils.createKeybindingHintDecoration).mockReturnValue(mockDecorationType);
    });

    const triggerSelectionChange = () =>
      triggerTextEditorSelectionChange({
        textEditor: mockEditor,
      } as vscode.TextEditorSelectionChangeEvent);

    it('shows hint when enabled and on empty line with correct range', async () => {
      const expectedPosition = new vscode.Position(2, 5);
      mockEditor.selection.active = expectedPosition;

      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).toHaveBeenCalled();
      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        mockEditor.selection,
      ]);
    });

    it('does not show hint when disabled', async () => {
      mockConfig.keybindingHints.enabled = false;
      await triggerConfigurationChange();
      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).not.toHaveBeenCalled();
      expect(mockEditor.setDecorations).not.toHaveBeenCalled();
    });

    it('does not show hint on non-empty lines', async () => {
      mockEditor.document.lineAt = jest.fn().mockReturnValue({ isEmptyOrWhitespace: false });
      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).not.toHaveBeenCalled();
      expect(mockEditor.setDecorations).not.toHaveBeenCalled();
    });

    it('does not show hint for non-file documents', async () => {
      mockEditor = createFakePartial<vscode.TextEditor>({
        selection: createFakePartial<vscode.Selection>({ active: new vscode.Position(0, 0) }),
        document: createFakePartial<vscode.TextDocument>({
          lineAt: jest.fn().mockReturnValue({ isEmptyOrWhitespace: true }),
          uri: vscode.Uri.parse('output:test'), // Create a non-file URI
        }),
        setDecorations: jest.fn(),
      });

      await triggerSelectionChange();

      expect(utils.createKeybindingHintDecoration).not.toHaveBeenCalled();
      expect(mockEditor.setDecorations).not.toHaveBeenCalled();
    });
  });

  describe('gutter icon', () => {
    let mockEditor: vscode.TextEditor;
    let mockDecorationType: vscode.TextEditorDecorationType;
    let mockUri: vscode.Uri;

    beforeEach(async () => {
      mockUri = vscode.Uri.file('/test.ts');
      mockDecorationType = createFakePartial<vscode.TextEditorDecorationType>({
        dispose: jest.fn(),
      });
      jest.mocked(utils.createGutterIconDecoration).mockReturnValue(mockDecorationType);

      mockEditor = createFakePartial<vscode.TextEditor>({
        document: { uri: mockUri },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });
      (vscode.window.activeTextEditor as unknown) = mockEditor;

      mockThread = createFakePartial<vscode.CommentThread>({
        uri: mockUri,
        range: new vscode.Range(0, 0, 1, 0),
        collapsibleState: vscode.CommentThreadCollapsibleState.Expanded,
      });
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);
    });

    it('shows gutter icon when chat is shown', async () => {
      await quickChat.show();

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        { range: new vscode.Range(mockThread.range.end, mockThread.range.end) },
      ]);
    });

    it('removes gutter icon when thread is collapsed', async () => {
      await quickChat.show();
      mockThread.collapsibleState = vscode.CommentThreadCollapsibleState.Collapsed;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: mockEditor,
            }),
          ),
        );

      expect(mockDecorationType.dispose).toHaveBeenCalled();
    });

    it('shows gutter icon when in comment input', async () => {
      await quickChat.show();

      // Create new mock editor with comment input URI
      const commentInputEditor = createFakePartial<vscode.TextEditor>({
        document: {
          uri: createFakePartial<vscode.Uri>({
            authority: COMMENT_CONTROLLER_ID,
            scheme: 'comment',
          }),
        },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });

      (vscode.window.visibleTextEditors as unknown) = [mockEditor];
      (vscode.window.activeTextEditor as unknown) = commentInputEditor;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: commentInputEditor,
            }),
          ),
        );

      expect(mockEditor.setDecorations).toHaveBeenCalledWith(mockDecorationType, [
        { range: new vscode.Range(mockThread.range.end, mockThread.range.end) },
      ]);
    });

    it('does not show gutter icon for different documents', async () => {
      await quickChat.show();

      const differentEditor = createFakePartial<vscode.TextEditor>({
        document: { uri: vscode.Uri.file('/different.ts') },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });
      (vscode.window.activeTextEditor as unknown) = differentEditor;

      jest
        .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
        .mock.calls.forEach(([listener]) =>
          listener(
            createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
              textEditor: differentEditor,
            }),
          ),
        );

      expect(differentEditor.setDecorations).not.toHaveBeenCalledWith(
        mockDecorationType,
        expect.any(Array),
      );
    });
  });

  describe('document changes', () => {
    beforeEach(async () => {
      mockThread = createFakePartial<vscode.CommentThread>({
        uri: vscode.Uri.file('/test.ts'),
        range: new vscode.Range(0, 0, 5, 10),
      });

      await showChatWithEditor();
    });

    it('updates thread position when text changes', () => {
      const change = {
        range: new vscode.Range(2, 0, 2, 0),
        text: 'new line\n',
      };

      const newRange = new vscode.Range(0, 0, 6, 10);
      jest.mocked(utils.calculateThreadRange).mockReturnValue(newRange);

      expect(mockThread.range.end.line).toBe(5);

      jest.mocked(vscode.workspace.onDidChangeTextDocument).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextDocumentChangeEvent>({
            document: createFakePartial<vscode.TextDocument>({ uri: mockThread.uri }),
            contentChanges: [change],
          }),
        ),
      );

      expect(mockThread.range.end.line).toBe(6);
    });
  });

  describe('update context key: `gitlab:quickChatOpen`', () => {
    let mockEditor: vscode.TextEditor;
    let mockUri: vscode.Uri;
    beforeEach(async () => {
      mockUri = vscode.Uri.file('/test.ts');
      mockEditor = createFakePartial<vscode.TextEditor>({
        document: { uri: mockUri },
        selection: new vscode.Selection(0, 0, 0, 0),
        setDecorations: jest.fn(),
      });
      (vscode.window.activeTextEditor as unknown) = mockEditor;

      jest.spyOn(vscode.window, 'showInputBox').mockResolvedValue('testing');

      mockThread = createFakePartial<vscode.CommentThread>({
        uri: mockUri,
        range: new vscode.Range(0, 0, 1, 0),
        collapsibleState: vscode.CommentThreadCollapsibleState.Expanded,
      });
    });

    it.each`
      message         | isExpanded | quickChatOpen
      ${'showing'}    | ${true}    | ${true}
      ${'collapsing'} | ${false}   | ${false}
    `(
      '$message quickChat updates the key to $quickChatOpen',
      async ({ isExpanded, quickChatOpen }) => {
        (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

        await quickChat.show();
        if (!isExpanded) {
          mockThread.collapsibleState = vscode.CommentThreadCollapsibleState.Collapsed;
        }

        jest
          .mocked(vscode.window.onDidChangeTextEditorVisibleRanges)
          .mock.calls.forEach(([listener]) =>
            listener(
              createFakePartial<vscode.TextEditorVisibleRangesChangeEvent>({
                textEditor: mockEditor,
              }),
            ),
          );

        expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
          'setContext',
          'gitlab:quickChatOpen',
          quickChatOpen,
        );
      },
    );

    it('update key to false if activeEditor document uri is not quickChat thread uri', async () => {
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);
      await quickChat.show();

      jest.mocked(vscode.window.onDidChangeActiveTextEditor).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextEditor>({
            document: { uri: vscode.Uri.file('/diffFile/path') },
          }),
        ),
      );

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:quickChatOpen',
        false,
      );
    });

    it('update key to true if activeEditor document uri is quickChat thread uri', async () => {
      const diffUri = vscode.Uri.file('/diffFile/path');
      mockThread = createFakePartial<vscode.CommentThread>({
        uri: diffUri,
        range: new vscode.Range(0, 0, 1, 0),
        collapsibleState: vscode.CommentThreadCollapsibleState.Expanded,
      });
      (mockCommentController.createCommentThread as jest.Mock).mockReturnValue(mockThread);

      await quickChat.show();

      jest.mocked(vscode.window.onDidChangeActiveTextEditor).mock.calls.forEach(([listener]) =>
        listener(
          createFakePartial<vscode.TextEditor>({
            document: { uri: diffUri },
          }),
        ),
      );

      expect(vscode.commands.executeCommand).toHaveBeenCalledWith(
        'setContext',
        'gitlab:quickChatOpen',
        true,
      );
    });
  });
});
