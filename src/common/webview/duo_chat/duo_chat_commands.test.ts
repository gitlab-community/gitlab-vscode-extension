import vscode from 'vscode';
import { WebviewMessageRegistry } from '../message_handlers';
import { DUO_CHAT_WEBVIEW_ID } from '../../constants';
import { getActiveFileContext, GitLabChatFileContext } from '../../chat/gitlab_chat_file_context';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { USER_COMMANDS } from '../../command_names';
import { registerDuoChatCommands } from './duo_chat_commands';
import { LSDuoChatWebviewController } from './duo_chat_controller';

jest.mock('../../chat/gitlab_chat_file_context');

describe('registerDuoChatCommands', () => {
  let webviewMessageRegistry: WebviewMessageRegistry;
  let sendNotificationMock: jest.Mock;
  let activeFileContext: GitLabChatFileContext;
  let chatController: LSDuoChatWebviewController;

  beforeEach(() => {
    sendNotificationMock = jest.fn();

    webviewMessageRegistry = createFakePartial<WebviewMessageRegistry>({
      sendNotification: sendNotificationMock,
    });

    chatController = createFakePartial<LSDuoChatWebviewController>({
      show: jest.fn(),
      hide: jest.fn(),
    });

    jest
      .mocked(vscode.commands.registerCommand)
      .mockReturnValue(createFakePartial<vscode.Disposable>({}));

    activeFileContext = createFakePartial<GitLabChatFileContext>({ fileName: 'test.js' });
    jest.mocked(getActiveFileContext).mockReturnValue(activeFileContext);
  });

  it('registers all Duo chat commands', async () => {
    const commands = [
      USER_COMMANDS.EXPLAIN_SELECTED_CODE,
      USER_COMMANDS.GENERATE_TESTS,
      USER_COMMANDS.REFACTOR_CODE,
      USER_COMMANDS.FIX_CODE,
      USER_COMMANDS.NEW_CHAT_CONVERSATION,
      USER_COMMANDS.CLOSE_CHAT,
    ];

    await registerDuoChatCommands(webviewMessageRegistry, chatController);

    commands.forEach(command => {
      expect(vscode.commands.registerCommand).toHaveBeenCalledWith(command, expect.any(Function));
    });
  });

  it('returns a disposable when registering Duo chat commands', async () => {
    const spy = jest.spyOn(vscode.Disposable, 'from');
    await registerDuoChatCommands(webviewMessageRegistry, chatController);
    const callArgs = spy.mock.calls[0];
    expect(callArgs).toHaveLength(7);
    expect(spy).toHaveBeenCalled();
  });

  describe('Running command', () => {
    const testCases = [
      { commandIndex: 0, promptType: 'explainCode', description: 'Explain selected code' },
      { commandIndex: 1, promptType: 'generateTests', description: 'Generate tests' },
      { commandIndex: 2, promptType: 'refactorCode', description: 'Refactor code' },
      { commandIndex: 3, promptType: 'fixCode', description: 'Fix code' },
      { commandIndex: 4, promptType: 'newConversation', description: 'New chat conversation' },
      { commandIndex: 5, promptType: 'focusChat', description: 'Focus chat' },
    ];

    test.each(testCases)(
      'sends correct notification when "$description" command is executed',
      async ({ commandIndex, promptType }) => {
        await registerDuoChatCommands(webviewMessageRegistry, chatController);
        const callCommand = jest.mocked(vscode.commands.registerCommand).mock.calls[
          commandIndex
        ][1];

        await callCommand();

        let context = {};

        if (promptType !== 'focusChat') {
          context = { fileContext: activeFileContext };
          expect(chatController.show).toHaveBeenCalled();
        }

        expect(sendNotificationMock).toHaveBeenCalledWith(DUO_CHAT_WEBVIEW_ID, 'newPrompt', {
          prompt: promptType,
          ...context,
        });
      },
    );

    it('Calls controller when "Close chat" command is executed', async () => {
      await registerDuoChatCommands(webviewMessageRegistry, chatController);
      const closeChatCommand = jest.mocked(vscode.commands.registerCommand).mock.calls[6][1];
      await closeChatCommand();
      expect(chatController.hide).toHaveBeenCalled();
    });
  });
});
