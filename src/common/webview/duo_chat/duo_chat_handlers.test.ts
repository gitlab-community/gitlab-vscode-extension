import * as vscode from 'vscode';
import { DUO_CHAT_WEBVIEW_ID } from '../../constants';
import { WebviewMessageRegistry } from '../message_handlers/webview_message_registry';
import { createFakePartial } from '../../test_utils/create_fake_partial';
import { log } from '../../log';
import {
  registerDuoChatHandlers,
  showUserMessage,
  MessageType,
  openLink,
} from './duo_chat_handlers';
import { LSDuoChatWebviewController } from './duo_chat_controller';

jest.mock('../setup_webviews');
jest.mock('vscode');

describe('registerDuoChatHandlers', () => {
  let mockRegistry: WebviewMessageRegistry;
  const chatController = createFakePartial<LSDuoChatWebviewController>({});

  beforeEach(() => {
    mockRegistry = createFakePartial<WebviewMessageRegistry>({
      onRequest: jest.fn(),
      onNotification: jest.fn(),
    });

    registerDuoChatHandlers(mockRegistry, chatController);
  });

  const notificationHandlers = [
    'insertCodeSnippet',
    'copyCodeSnippet',
    'appReady',
    'focusChange',
    'showMessage',
    'openLink',
  ];

  it('should register a request handler for `getCurrentFileContext` request', () => {
    expect(mockRegistry.onRequest).toHaveBeenCalledWith(
      DUO_CHAT_WEBVIEW_ID,
      'getCurrentFileContext',
      expect.any(Function),
    );
  });

  notificationHandlers.forEach(handler => {
    it(`should register a handler for \`${handler}\` notification`, () => {
      expect(mockRegistry.onNotification).toHaveBeenCalledWith(
        DUO_CHAT_WEBVIEW_ID,
        handler,
        expect.any(Function),
      );
    });
  });
  type ShowMessageMethod = 'showInformationMessage' | 'showWarningMessage' | 'showErrorMessage';

  describe('showUserMessage', () => {
    it.each`
      messageType  | expectedMethod
      ${'info'}    | ${'showInformationMessage'}
      ${'warning'} | ${'showWarningMessage'}
      ${'error'}   | ${'showErrorMessage'}
    `(
      'when messageType is "$messageType" calls "vscode.window.$expectedMethod"',
      async ({
        messageType,
        expectedMethod,
      }: {
        messageType: MessageType;
        expectedMethod: ShowMessageMethod;
      }) => {
        const spy = jest.spyOn(vscode.window, expectedMethod);

        const message = 'Hi, user!';
        await showUserMessage({ type: messageType, message });

        expect(spy).toHaveBeenCalledWith(message);
      },
    );
  });

  describe('openLink', () => {
    let uriParseSpy: jest.SpyInstance;
    let openExternalUrlSpy: jest.SpyInstance;

    beforeEach(() => {
      uriParseSpy = jest.spyOn(vscode.Uri, 'parse');
      openExternalUrlSpy = jest.spyOn(vscode.env, 'openExternal');
    });

    it('should call vscode API to parse and open external link', async () => {
      const href = 'https://google.com';
      await openLink({ href });
      expect(uriParseSpy).toHaveBeenCalledWith(href);
      expect(openExternalUrlSpy).toHaveBeenCalledWith(expect.any(vscode.Uri));
    });

    it('should log a warning when opening link failed', async () => {
      jest.mocked(openExternalUrlSpy).mockResolvedValueOnce(false);
      jest.spyOn(log, 'warn');
      const href = 'https://google.com';
      await openLink({ href });
      expect(log.warn).toHaveBeenCalledWith(`Failed to open URL: ${href}`);
    });
  });
});
