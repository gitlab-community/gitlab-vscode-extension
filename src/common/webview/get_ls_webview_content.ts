import { IS_OSX } from '../constants';
import { escapeHtml } from './escape_html';
import html from './templates/webview.template.html';
import osxKeyboardEventScript from './templates/osx_keyboard_event_fix.template.html';

export const getWebviewContent = (url: URL, title: string) => {
  const safeTitle = escapeHtml(title);
  const safeOrigin = escapeHtml(url.origin);
  const safeUrl = encodeURI(url.toString());

  let webviewHtml = html
    .replace(/{{origin}}/g, safeOrigin)
    .replace(/{{title}}/g, safeTitle)
    .replace(/{{url}}/g, safeUrl);

  if (IS_OSX) {
    webviewHtml = webviewHtml.replace('</head>', `${osxKeyboardEventScript}</head>`);
  }

  return webviewHtml;
};
