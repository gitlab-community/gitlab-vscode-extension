export const VS_COMMANDS = {
  DIFF: 'vscode.diff',
  OPEN: 'vscode.open',
  GIT_SHOW_OUTPUT: 'git.showOutput',
  GIT_CLONE: 'git.clone',
  MARKDOWN_SHOW_PREVIEW: 'markdown.showPreview',
  OPEN_SETTINGS: 'workbench.action.openSettings',
};

export const USER_COMMANDS = {
  AUTHENTICATE: 'gl.authenticate',
  SHOW_LOGS: 'gl.showOutput',
  SHOW_DIAGNOSTICS: 'gl.showDiagnostics',
  // Duo Chat WebView commands
  CLOSE_CHAT: 'gl.webview.closeChat',
  EXPLAIN_SELECTED_CODE: 'gl.webview.explainSelectedCode',
  GENERATE_TESTS: 'gl.webview.generateTests',
  REFACTOR_CODE: 'gl.webview.refactorCode',
  FIX_CODE: 'gl.webview.fixCode',
  NEW_CHAT_CONVERSATION: 'gl.webview.newChatConversation',
  FOCUS_CHAT: 'gl.webview.focusChat',
  DUO_TUTORIAL: 'gl.duoTutorial',
};
