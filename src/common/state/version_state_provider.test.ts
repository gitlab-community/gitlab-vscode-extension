import { LanguageServerManager } from '../language_server/language_server_manager';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { VersionStateProvider, VersionDetails } from './version_state_provider';

describe('VersionStateProvider', () => {
  const workflowVersion = '3.4.5';
  let versionStateProvider: VersionStateProvider;
  const lsVersionProvider = createFakePartial<LanguageServerManager>({
    onChange: jest.fn(),
    version: '3.3.3',
  });

  beforeEach(() => {
    versionStateProvider = new VersionStateProvider(workflowVersion, lsVersionProvider);
  });

  it('should construct the correct versionStateProvider', () => {
    const expectedState: VersionDetails = {
      vscodeAppName: 'test-app-name',
      vscodeVersion: 'vscode-test-version-0.0',
      extensionVersion: workflowVersion,
      languageServerVersion: lsVersionProvider.version,
    };

    expect(versionStateProvider.state).toEqual(expectedState);
  });

  it('constructs the VersionStateProvider when languageServerVersion is undefined', () => {
    versionStateProvider = new VersionStateProvider(workflowVersion, undefined);
    const expectedState: VersionDetails = {
      vscodeAppName: 'test-app-name',
      vscodeVersion: 'vscode-test-version-0.0',
      extensionVersion: workflowVersion,
      languageServerVersion: undefined,
    };
    expect(versionStateProvider.state).toEqual(expectedState);
  });
});
