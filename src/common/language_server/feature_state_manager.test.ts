import {
  CHAT,
  CODE_SUGGESTIONS,
  FeatureState,
  FeatureStateCheck,
  CHAT_NO_LICENSE,
} from '@gitlab-org/gitlab-lsp';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { FeatureStateManager } from './feature_state_manager';

describe('FeatureStateManager', () => {
  let featureStateManager: FeatureStateManager;

  beforeEach(() => {
    featureStateManager = new FeatureStateManager();
  });

  it('should initialize with an empty state', () => {
    expect(featureStateManager.state).toEqual([]);
  });

  it('should update state and emit an event when "setStates" is called', () => {
    const mockStates: FeatureState[] = [{ featureId: CHAT, engagedChecks: [] }];

    featureStateManager.setStates(mockStates);

    expect(featureStateManager.state).toEqual(mockStates);
  });

  it('should call "onChange" listener with the initial state when listener is added', () => {
    const listener = jest.fn();
    const mockStates: FeatureState[] = [
      {
        featureId: CHAT,
        engagedChecks: [
          createFakePartial<FeatureStateCheck<typeof CHAT_NO_LICENSE>>({
            details: 'No chat license.',
          }),
        ],
      },
    ];
    featureStateManager.setStates(mockStates);
    expect(listener).not.toHaveBeenCalled();

    featureStateManager.onChange(listener);

    expect(listener).toHaveBeenCalledWith(mockStates);
  });

  it('should call "onChange" listener with the new state when state is updated', () => {
    const listener = jest.fn();

    featureStateManager.onChange(listener);
    const mockStates: FeatureState[] = [
      { featureId: CHAT, engagedChecks: [] },
      { featureId: CODE_SUGGESTIONS, engagedChecks: [] },
    ];
    featureStateManager.setStates(mockStates);
    expect(listener).toHaveBeenCalledWith(mockStates);
  });
});
